package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	_ "github.com/lib/pq"
)

const (
	TG_APITOKEN = "6009714842:AAFALmiXIrSpazXSvfIk9bDM5faik5ecLTg"
)

const (
	host     = "localhost"
	port     = 5432
	user     = "postgres"
	password = "postgres"
	dbname   = "wod_db"
)

func main() {
	bot, err := tgbotapi.NewBotAPI(TG_APITOKEN)
	if err != nil {
		log.Panic(err)
	}
	// -----------connect to db
	// connection string
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	// open database
	db, err := sql.Open("postgres", psqlconn)
	if err != nil {
		log.Panic(err)
	}
	// close database
	defer db.Close()
	// check db
	err = db.Ping()
	if err != nil {
		log.Panic(err)
	}

	log.Printf("Connected to DB!")

	// -----------connect to db

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	current_day := time.Now()

	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message != nil { // If we got a message
			log.Printf("[%s] %s", update.Message.From.UserName, update.Message.Text)

			msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)
			switch update.Message.Command() {
			case "help":
				msg.Text = "Я еще не умею ничего больше кроме команды /wod"
			case "wod":
				msg.Text = "Привет, " + update.Message.From.FirstName + "\n" + "Твоё задание на день: "
				randWodId := rand.Intn(100)
				sqlStatement := `select "username", "wod_date" from "wod_users" where wod_date = $1`
				wod_user := db.QueryRow(sqlStatement, current_day)

				var username string
				var wod_date string
				var wod string
				var wod_link string

				err = wod_user.Scan(&username, &wod_date)
				if err != nil {
					//first call of the day
					sqlStatement = `select "wod", "wod_link" from "wod" where wod_id = $1`
					wod_data := db.QueryRow(sqlStatement, randWodId)
					err = wod_data.Scan(&wod, &wod_link)
					if err != nil {
						//нет заданий в базе, обратитесь к администратору
						log.Panic(err)
					}
					msg.Text = msg.Text + "\n" + wod + "\n" + wod_link
				}

				/*
					rows, err := db.Query(`select "firstname", "wod_date" from "wod_users" order by random() limit 1`)
					if err != nil {
						log.Panic(err)
					}

					defer rows.Close()
					for rows.Next() {
						var firstname string
						var wod_date string

						err = rows.Scan(&firstname, &wod_date)
						if err != nil {
							log.Panic(err)
						}

						msg.Text = msg.Text + "\n" + wod
					}
				*/

			/*
				case "set_wod":
					msg.Text = "Задание сохранено. Спасибо!"
					// insert (dynamic)
					insertDynStmt := `insert into "clients"("name", "wos") values($1, $3)`
					_, err = db.Exec(insertDynStmt, update.Message.From.UserName, "50 отжиманий")
					if err != nil {
						log.Panic(err)
					}
			*/
			default:
				msg.Text = "Всем привет! Я еще не умею ничего больше кроме команды /wod"
			}

			if _, err := bot.Send(msg); err != nil {
				log.Panic(err)
			}
		}
	}
}
